// var axios = require("axios");
const contentful = require("contentful");
const stringify = require('json-stringify-safe');

const getNewsData = (isDevContent = false, username = '', password = '') => {
    return new Promise((resolve, reject) => {
        if (!isDevContent || (username == '' && password == '')) {
            
            console.log("pass");
            var contentfulClient = contentful.createClient(isDevContent ? {
                accessToken: '4824fc75affafd58c9ca765ae8e0f328291557d4be99484a9971031d4e768070',
                space: '0o6s67aqvwnu',
                host: "preview.contentful.com"
            }
                :
                {
                    accessToken: 'd998d5244bc938e8f135584d86293026912988fd3c730c439abe26adabbc3edb',
                    space: '0o6s67aqvwnu',
                })

            contentfulClient
                .getEntries({
                    content_type: "postBase",
                    include: 3
                })
                .then((entries) => {
                    resolve(entries);
                })
                .catch((err) => {
                    reject(err);
                })
        } else {
            console.log("reject");
            reject({ err: 'username and password do not match the development secret' });
        }
    })
}

const returnResult = function (statusCode, body) {
    const response = {
        statusCode: statusCode,
        body: body ? stringify(body) : body,
        headers: {
            "Access-Control-Allow-Origin": "*", // Required for CORS support to work
            "Access-Control-Allow-Credentials": true // Required for cookies, authorization headers with HTTPS 
        },
    };
    return response;
}

exports.handler = (event, context, callback) => {

    if (event.queryStringParameters && event.queryStringParameters.isDevContent && event.queryStringParameters.isDevContent == 'true') {
        console.log(event.queryStringParameters);
        // if (event.queryStringParameters.username &&
        //     event.queryStringParameters.username !== "" &&
        //     event.queryStringParameters.password &&
        //     event.queryStringParameters.password !== "") {
            getNewsData(true,
                event.queryStringParameters.username ? event.queryStringParameters.username : '',
                event.queryStringParameters.password ? event.queryStringParameters.password : '')
                .then((contentData) => {
                    callback(null, returnResult(200, contentData));
                })
                .catch((err) => {
                    callback(err, null);
                })
        // } else {
        //     callback({ err: 'empty username or password' }, null);
        // }
    } else {
        getNewsData(false, '', '')
            .then((contentData) => {
                callback(null, returnResult(200, contentData));
            })
            .catch((err) => {
                callback(err, null);
            })
    }

};
